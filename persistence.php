<?php
require 'Autoloader.php';

$usersi = new Acd\Service\UserService();

// save new user
$inputFromUser = array('firstname'=>'aaron', 'lastname'=>'saray', 'email'=>'me@me.com');
try {
	$user = $usersi->createFromUserInput($inputFromUser);
	$usersi->save($user);
}
catch (\Exception $e) {
	print $e->getMessage();
}

// save new user, invalid data
$inputFromUser = array('firstname'=>'aaron', 'lastname'=>'', 'email'=>'me@me.com');
try {
	$user = $usersi->createFromUserInput($inputFromUser);
	$usersi->save($user);
}
catch (\Exception $e) {
	// would go to exception, deal with it how you'd like
	print $e->getMessage();
}

//update existing user with data
$user          = $usersi->findOneById(1);
$inputFromUser = array('firstname'=>'Joe');
try {
	$usersi->applyUpdateFromUserInput($user, $inputFromUser);
	$usersi->save($user);
}
catch (\Exception $e) {
	print $e->getMessage();
}