<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acd\Mapper;

/**
 * Description of UserInterface
 *
 * @author Acidvertigo
 */

interface DataMapperInterface
{
    public function fetchById($id, $table);
    public function fetchAll(array $conditions = array(), $table);
}