<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acd\Mapper;

/**
 * Description of User
 *
 * @author Acidvertigo
 */

class DataMapper implements DataMapperInterface {

    /** @var \PDO */
    protected $conn = null;

    /** @var string */
    protected $entityTable = 'user';

    /** @var object */
    protected $collection;

    /**
     * 
     * @param \PDO $adapter
     * @param \Acd\Model\CollectionInterface $collection
     */
    public function __construct(\PDO $adapter, \Acd\Model\CollectionInterface $collection) {
        $this->conn       = $adapter;
        $this->collection = $collection;
    }

    /**
     * 
     * @param array $id
     * @return null|object
     */
    public function fetchById($id, $table) {
        $st = $this->conn->prepare('select * from ' . $table . ' where id = :id');
        $st->execute(array(':id' => $id));

        if (!$row = $st->fetch()) {
            return null;
        }

        return $this->createModel($row, $table);
    }

    /**
     * 
     * @param array $params
     * @return object
     */
    public function fetchAll(array $params = array(), $table) {

        $whereStrings       = $whereParams = array();

        if(!empty($params)) {
            foreach ($params as $key => $value) {
                $whereStrings[] = $key . ' = ?';
                $whereParams[]  = $value;
            }
        }

        $sql = 'select * from ' . $table;

        if (isset($whereStrings) && !empty($whereStrings)) {
            $sql .= ' where ' . implode(' AND ', $whereStrings);
        }

        if (isset($params['limit'])) {
            $sql .= ' limit ' . (int)($params['limit']);
        }

        $statement = $this->conn->prepare($sql);
        $statement->execute($whereParams);

        $results = $statement->fetchAll();

        return $this->createUserCollection($results, $table);
    }

    /**
     * 
     * @param array $array
     * @param string $class
     * @throw \Exception
     * @return object
     */
    protected function createModel(array $array, $class) {

        $classname = '';

        if($class !== null) {
            $classname = 'Acd\Model' . '\\' . $class. '\\' . $class . 'Model';
        }

        if(class_exists($classname)) {
            $object = new $classname();
        } else {
            throw new \Exception('no valid class');
        }

        if(!empty($array)) {
            foreach ($array as $key => $value) {
                $object->$key = $value;
            }
        }

        return $object;
    }

    /**
     * 
     * @param array $rows
     * @return object
     */
    protected function createUserCollection(array $rows, $class) {
        $this->collection->clear();

        if (!empty($rows) && $class !== null) {
            foreach ($rows as $row) {
                $this->collection[] = $this->createModel($row, $class);
            }
        }
        return $this->collection;
    }

    /**
     * 
     * @param \Acd\Model\UserModel $user
     */
    public function save(\Acd\Model\UserModel $user) {
        if ($user->id) {
            $sql    = 'update user set firstname = ?, lastname = ?, email = ? where id = ?';
            $params = array($user->firstname, $user->lastname, $user->email, $user->id);
        } else {
            $sql    = 'insert into user (firstname, lastname, email) values (?, ?, ?)';
            $params = array($user->firstname, $user->lastname, $user->email);
        }

        $statement = $this->conn->prepare($sql);
        $statement->execute($params);
    }

}