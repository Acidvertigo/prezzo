<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acd\Model;

/**
 * Description of UserCollection
 *
 * @author Acidvertigo
 */
class Collection implements CollectionInterface, \Countable, \IteratorAggregate, \ArrayAccess
{

    /** @var array $users */
    protected $users = array();

    /**
     * 
     * @param mixed $key
     * @param \Acd\Model\ModelInterface $model
     */
    public function add($key, ModelInterface $model)
    {
        $this->offsetSet($key, $model);
    }

    /**
     * 
     * @param \Acd\Model\ModelInterface $model
     */
    public function remove(ModelInterface $model)
    {
        $this->offsetUnset($model);
    }

    /**
     * 
     * @param mixed $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->offsetGet($key);
    }

    /**
     * 
     * @param mixed $key
     * @return boolean
     */
    public function exists($key)
    {
        return $this->offsetExists($key);
    }

    public function clear()
    {
        $this->users = array();
    }

    /**
     * 
     * @return array
     */
    public function toArray()
    {
        return $this->users;
    }

    /**
     * 
     * @return int
     */
    public function count()
    {
        return count($this->users);
    }

    public function offsetSet($key, $value)
    {

        if (!$value instanceof ModelInterface) {
            throw new \InvalidArgumentException(
            'Could not add the user to the collection.');
        }

        if (!isset($key)) {
            $this->users[] = $value;
        } else {
            $this->users[$key] = $value;
        }
    }

    public function offsetUnset($key)
    {
        if ($key instanceof ModelInterface) {
            $this->users = array_filter($this->users, function ($v) use ($key) {
                return $v !== $key;
            });

            if (isset($this->users[$key])) {
                unset($this->users[$key]);
            }
        }
    }

    public function offsetGet($key)
    {
        if (isset($this->users[$key])) {
            return $this->users[$key];
        }
    }

    public function offsetExists($key)
    {
        if (!$key instanceof ModelInterface) {
            throw new \InvalidArgumentException(
            'Could not verify the key for this collection.');
        }

        return isset($this->users[$key]);
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->users);
    }

}