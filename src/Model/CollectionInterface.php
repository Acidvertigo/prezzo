<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Acd\Model;

/**
 * Description of UserCollectionInterface
 *
 * @author Acidvertigo
 */
interface CollectionInterface {

    public function add($key = null, ModelInterface $user);

    public function remove(ModelInterface $user);

    public function get($key);

    public function exists($key);

    public function clear();

    public function toArray();

    public function count();

    public function offsetSet($key, $value);

    public function offsetUnset($key);

    public function offsetGet($key);

    public function offsetExists($key);

    public function getIterator();

}