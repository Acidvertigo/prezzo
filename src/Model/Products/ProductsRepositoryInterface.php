<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acd\Model\Products;

/**
 * Description of UserRepositoryInterface
 *
 * @author Acidvertigo
 */
interface ProductsRepositoryInterface
{
    public function fetchById($id);
    public function fetchByModel($model);
    public function fetchbyPrice($image);
    public function fetchByStatus($status);
}