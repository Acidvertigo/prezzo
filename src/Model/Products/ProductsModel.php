<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acd\Model\Products;

use Acd\Model\ModelInterface;

/**
 * Description of newPHPClass
 *
 * @author Acidvertigo
 */
class ProductsModel implements ModelInterface {

    /** @var int $products_id */
    public $products_id;

    /** @var string $products_quantity */
    public $products_quantity;

    /** @var string $products_model */
    public $products_model;

    /** @var decimal $products_price */
    public $products_price;

    /** @var string $products_image */
    public $products_image;

}