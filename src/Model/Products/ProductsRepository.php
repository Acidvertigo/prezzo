<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acd\Model\Products;

/**
 * Description of UserRepository
 *
 * @author Acidvertigo
 */
class ProductsRepository implements ProductsRepositoryInterface
{
    /** @var \Acd\Mapper\DataMapperInterface */
    protected $datamapper;
    
    /** @var string $table */
    protected $table = 'Products';

    /**
     * 
     * @param \Acd\Mapper\DataMapperInterface $userMapper
     */
    public function __construct(\Acd\Mapper\DataMapperInterface $userMapper) {
        $this->datamapper = $userMapper;
    }

    /**
     * 
     * @param int $id
     * @return type
     */
    public function fetchById($id) {
        return $this->datamapper->fetchById($id, $this->table);
    }
    
    /**
     * 
     * @param string $model
     * @return array
     */
    public function fetchByModel($model) {
        return $this->fetch(array('products_model' => $model));
    }
    
    /**
     * 
     * @param string $price
     * @return array
     */
    public function fetchByPrice($price) {
        return $this->fetch(array('products_price' => $price));
    }

    /**
     * 
     * @param string $status
     * @return array
     */
    public function fetchbyStatus($status) {
        return $this->fetch(array('products_status' => $status));
    }

    /**
     * 
     * @param array $params
     * @return array
     */
    protected function fetch(array $params) {
        return $this->datamapper->fetchAll($params, $this->table);
    }
}