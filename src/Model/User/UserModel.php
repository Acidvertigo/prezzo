<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acd\Model\User;

use Acd\Model\ModelInterface;

/**
 * Description of newPHPClass
 *
 * @author Acidvertigo
 */
class UserModel implements ModelInterface {
    
    /** @var int $type */
    public $id;

    /** @var string $firstname */
    public $firstname;

    /** @var string $lastname */
    public $lastname;

    /** @var string $email */
    public $email;

    /** @var string $datecreated */
    public $datecreated;

}