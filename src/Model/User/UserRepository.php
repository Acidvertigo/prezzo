<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Acd\Model\User;

/**
 * Description of UserRepository
 *
 * @author Acidvertigo
 */
class UserRepository implements UserRepositoryInterface
{
    /** @var \Acd\Mapper\DataMapperInterface */
    protected $datamapper;
    
    /** @var string $table */
    protected $table = 'User';

    /**
     * 
     * @param \Acd\Mapper\DataMapperInterface $dataMapper
     */
    public function __construct(\Acd\Mapper\DataMapperInterface $dataMapper) {
        $this->datamapper = $dataMapper;
    }

    /**
     * 
     * @param int $id
     * @return type
     */
    public function fetchById($id) {
        return $this->datamapper->fetchById($id, $this->table);
    }
    
    /**
     * 
     * @param string $name
     * @return array
     */
    public function fetchByName($name) {
        return $this->fetch(array('firstname' => $name));
    }
    
    /**
     * 
     * @param string $email
     * @return array
     */
    public function fetchByEmail($email) {
        return $this->fetch(array('email' => $email));
    }

    /**
     * 
     * @param string $role
     * @return array
     */
    public function fetchByRole($role) {
        return $this->fetch(array('role' => $role));
    }

    /**
     * 
     * @param array $params
     * @return array
     */
    protected function fetch(array $params) {
        return $this->datamapper->fetchAll($params, $this->table);
    }
}